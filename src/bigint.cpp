#include <cstring>
#include <climits>
#include <tuple>
#include "bigint.h"

using namespace std;

#define BASE 0xFFFFFFFF
int karatsuba_limit = 40;

mag_t* add_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size);
mag_t* subtract_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size);
mag_t* optimaly_multiply_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size);
mag_t* multiply_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size);
mag_t* multiply_karatsuba_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size);
tuple<mag_t *, mag_t *> divide_knuth_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len,
                                                     len_t& q_len, len_t& r_len);
int compare_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len);
len_t striped_zeros_len(len_t n, const mag_t* mag);

#define max(x, y) ((x) > (y) ? (x) : (y))
int divmnu(unsigned q[], unsigned r[],
           const unsigned u[], const unsigned v[],
           int m, int n);
int nlz(unsigned int x);

BigInt::BigInt() : BigInt(0) { }

BigInt::BigInt(int x) {
    this->signum = (x > 0) - (x < 0);
    this->len = this->signum != 0;
    if (this->signum == 0) {
        this->mag_ptr = nullptr;
    } else {
        this->mag_ptr.reset(new mag_t { (mag_t) ((uint)x & 0x8FFFFFFF) });
    }
}

BigInt::BigInt(uint x) {
    this->signum = x > 0;
    this->len = this->signum != 0;
    if (this->signum == 0) {
        this->mag_ptr = nullptr;
    } else {
        this->mag_ptr.reset(new mag_t { (mag_t) x });
    }
}

BigInt::BigInt(long x) {
    this->signum = (x > 0) - (x < 0);
    this->len = (this->signum != 0) * (x > UINT_MAX ? 2 : 1);
    if (this->len == 0) {
        this->mag_ptr = nullptr;
    } else if (this->len == 1) {
        this->mag_ptr.reset(new mag_t { (mag_t) ((ulong)x & 0xFFFFFFFF) });
    } else {
        this->mag_ptr.reset(new mag_t[2] {
            (mag_t) ((ulong)x & 0xFFFFFFFF),
            (mag_t) (((ulong)x >> 32) & 0x8FFFFFFF)
        });
    }
}

BigInt::BigInt(ulong x) {
    this->signum = x > 0;
    this->len = (this->signum != 0) * (x > UINT_MAX ? 2 : 1);
    if (this->len == 0) {
        this->mag_ptr = nullptr;
    } else if (this->len == 1) {
        this->mag_ptr.reset(new mag_t { (mag_t) ((ulong)x & 0xFFFFFFFF) });
    } else {
        this->mag_ptr.reset(new mag_t[2] {
            (mag_t) ((ulong)x & 0xFFFFFFFF),
            (mag_t) ((ulong)x >> 32)
        });
    }
}

BigInt::BigInt(signum_t signum, mag_t mag) {
    this->signum = signum;
    this->len = this->signum != 0;
    if (this->signum == 0) {
        this->mag_ptr = nullptr;
    } else {
        this->mag_ptr.reset(new mag_t { mag });
    }
}

BigInt::BigInt(signum_t signum, len_t n, const mag_t *mag) {
    this->signum = (int)(signum > 0) - (int)(signum < 0);
    this->len = striped_zeros_len(n, mag);
    if (this->signum == 0 || this->len == 0) {
        this->signum = this->len = 0;
        this->mag_ptr = nullptr;
    } else {
        this->mag_ptr.reset(new mag_t[this->len]);
        memcpy((wchar_t *) this->mag(), (wchar_t *) mag, sizeof(mag_t) * this->len);
    }
}

BigInt::BigInt(signum_t signum, len_t n, shared_ptr<const mag_t> mag_ptr) {
    this->signum = (int)(signum > 0) - (int)(signum < 0);
    this->len = striped_zeros_len(n, mag());
    if (this->signum == 0 || this->len == 0) {
        this->signum = this->len = 0;
        this->mag_ptr = nullptr;
    } else {
        this->mag_ptr = mag_ptr;
    }
}

BigInt::BigInt(const BigInt& other) {
    this->signum = other.signum;
    this->len = other.len;
    this->mag_ptr = other.mag_ptr;
}

BigInt operator+(BigInt a, BigInt b) {
    if (a.signum == 0) return b;
    if (b.signum == 0) return a;
    if (a.signum == b.signum) {
        len_t len;
        mag_t* mag = add_mag(a.mag(), a.len, b.mag(), b.len, len);
        return BigInt(a.signum, len, mag);
    } else {
        int signum = compare_mag(a.mag(), a.len, b.mag(), b.len);
        if (signum == 0) return *new BigInt(0);
        if (signum < 0) {
            BigInt& c = a;
            a = b;
            b = c;
        }
        len_t len;
        mag_t* mag = subtract_mag(a.mag(), a.len, b.mag(), b.len, len);
        return BigInt(signum, len, mag);
    }
}

BigInt operator-(BigInt a, BigInt b) {
    if (a.signum == 0) return BigInt(-b.signum, b.len, b.mag());
    if (b.signum == 0) return a;
    if (a.signum == b.signum) {
        int signum = compare_mag(a.mag(), a.len, b.mag(), b.len);
        if (signum == 0) return *new BigInt(0);
        // TODO(yin): if (signum < 0) swap(a, b); and don't branch for substract_mag()
        len_t len;
        mag_t* mag;
        if (signum < 0) {
            mag = subtract_mag(b.mag(), b.len, a.mag(), a.len, len);
        } else {
            mag = subtract_mag(a.mag(), a.len, b.mag(), b.len, len);
        }
        return BigInt(a.signum*signum, len, mag);
    } else {
        len_t len;
        mag_t* mag = add_mag(a.mag(), a.len, b.mag(), b.len, len);
        return BigInt(-1*a.signum*b.signum, len, mag);
    }
}

BigInt operator*(BigInt a, BigInt b) {
    if (a.signum == 0 || b.signum == 0) return *new BigInt(0);
    if (a.signum == 1 && a.len == 1 && a.mag()[0] == 1) return b;
    if (b.signum == 1 && b.len == 1 && b.mag()[0] == 1) return a;
    len_t len;
    mag_t* mag = optimaly_multiply_mag(a.mag(), a.len, b.mag(), b.len, len);
    return BigInt(a.signum*b.signum, len, mag);
}

BigInt operator/(BigInt a, BigInt b) {
    len_t q_len, r_len;
    tuple<mag_t*, mag_t*> quotientAndReminder = divide_knuth_mag(a.mag(), a.len, b.mag(), b.len, q_len, r_len);
    mag_t *q_mag = get<0>(quotientAndReminder);
    mag_t *r_mag = get<1>(quotientAndReminder);
    free(r_mag);
    q_len = striped_zeros_len(q_len, q_mag);
    return BigInt(a.signum * b.signum, q_len, q_mag);
}

BigInt operator%(BigInt a, BigInt b) {
    len_t q_len, r_len;
    tuple<mag_t*, mag_t*> quotientAndReminder = divide_knuth_mag(a.mag(), a.len, b.mag(), b.len, q_len, r_len);
    mag_t *q_mag = get<0>(quotientAndReminder);
    mag_t *r_mag = get<1>(quotientAndReminder);
    free(q_mag);
    r_len = striped_zeros_len(r_len, r_mag);
    return BigInt(a.signum, r_len, r_mag);
}

pair<BigInt, BigInt> BigInt::divide(BigInt b) {
    len_t q_len, r_len;
    tuple<mag_t*, mag_t*> quotientAndReminder = divide_knuth_mag(mag(), len, b.mag(), b.len, q_len, r_len);
    mag_t *q_mag = get<0>(quotientAndReminder);
    mag_t *r_mag = get<1>(quotientAndReminder);
    q_len = striped_zeros_len(q_len, q_mag);
    r_len = striped_zeros_len(r_len, r_mag);
    return make_pair(BigInt(signum * b.signum, q_len, q_mag), BigInt(signum, r_len, r_mag));
}

longmag_t BigInt::toLongmag(long mask) {
    if (signum == 0) {
        return 0;
    }
    longmag_t longPart = (long) mag()[0];
    if (len >= 1) {
        longPart += (longmag_t) mag()[1] << sizeof(mag_t);
    }
    return longPart & mask;
}

mag_t BigInt::toIntmag(int mask) {
    if (signum == 0) {
        return 0;
    }
    return mag()[0] & mask;
}

char BigInt::toChar() {
    return signum * toIntmag(0x7F);
}

unsigned char BigInt::toUChar() {
    return toIntmag(0xFF);
}

char BigInt::toShort() {
    return signum * toIntmag(0x7FFF);
}

unsigned char BigInt::toUShort() {
    return toIntmag(0xFFFF);
}

int BigInt::toInt() {
    return signum * toIntmag(0x7FFFFFFF);
}

unsigned int BigInt::toUInt() {
    return toIntmag(0x7FFFFFFF);
}

long BigInt::toLong() {
    return signum * toLongmag(0x7FFFFFFFFFFFFFFF);
}

unsigned long BigInt::toULong() {
    return toLongmag(0xFFFFFFFFFFFFFFFF);
}

mag_t* add_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size) {
    size = max(a_len, b_len)+1;
    mag_t *mag = new mag_t[size];
    longmag_t c = 0;
    for (int i = 0; i < size; i++) {
        if (i < a_len) c += a_mag[i];
        if (i < b_len) c += b_mag[i];
        mag[i] = c & BASE;
        c >>= sizeof(mag_t) * 8;
    }
    size = striped_zeros_len(size, mag);
    return mag;
}

/*
 * Subtracts 2 unsigned integer arrays: A - B, where A > B must be true. Only subtracts the B-length
 * of elements (B-length + 1 if the last carries), the rest of A is copied over.
 */
mag_t* subtract_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& size) {
    size = max(a_len, b_len);
    mag_t *mag = new mag_t[size];
    longmag_t c = 0;
    for (int i = 0; i < size; i++) {
        if ( i < a_len && i < b_len) {
            c = (longmag_t) a_mag[i] - b_mag[i] - c;
        } else if (i < a_len) {
            c = (longmag_t) a_mag[i] - c;
        } else if (i < a_len) {
            c = (longmag_t) 0 - b_mag[i] - c;
        }
        mag[i] = c & BASE;
        c = c >> (sizeof(longmag_t) * 8 - 1) & 0x1;
    }
    size = striped_zeros_len(size, mag);
    return mag;
}

mag_t* optimaly_multiply_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& len) {
    int min_len = min(a_len, b_len);
    if (min_len < karatsuba_limit) {
        return multiply_mag(a_mag, a_len, b_mag, b_len, len);
    } else {
        return multiply_karatsuba_mag(a_mag, a_len, b_mag, b_len, len);
    }
}

/*
 * Multiplies 2 unsigned integer arrays with N and M elements. Complexity O(N*M)
 */
mag_t* multiply_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& len) {
    len = a_len + b_len;
    mag_t *mag = new mag_t[len];
    for (int i = 0; i < len; i++) mag[i] = 0;
    for (int a = 0; a < a_len; a++) {
        longmag_t c = 0;
        for (int b = 0; b < b_len; b++) {
            c = (longmag_t) a_mag[a] * b_mag[b] + mag[a+b] + c;
            mag[a+b] = c & BASE;
            c >>= 8 * (sizeof(longmag_t) - sizeof(mag_t));
        }
        if (c > 0) {
            mag[a + b_len] = c;
        }
    }
    len = striped_zeros_len(len, mag);
    return mag;
}

/*
 * Multiplies 2 unsigned integer arrays using the divide-and-conquer Karatsuba algorithm. Complexity O(N^~1.5849)
 */
mag_t* multiply_karatsuba_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len, len_t& len) {
    int min_len = min(a_len, b_len);
    int half_len = min_len / 2;
    len_t a0_len = half_len;
    const mag_t *a0_mag = a_mag;
    len_t a1_len = a_len - half_len;
    const mag_t *a1_mag = a_mag + half_len;
    len_t b0_len = half_len;
    const mag_t *b0_mag = b_mag;
    len_t b1_len = b_len - half_len;
    const mag_t *b1_mag = b_mag + half_len;

    len_t z0_len;
    mag_t *z0_mag = optimaly_multiply_mag(a0_mag, a0_len, b0_mag, b0_len, z0_len);
    len_t z2_len;
    mag_t *z2_mag = optimaly_multiply_mag(a1_mag, a1_len, b1_mag, b1_len, z2_len);

    len_t sa_len;
    mag_t *sa_mag = add_mag(a0_mag, a0_len, a1_mag, a1_len, sa_len);

    len_t sb_len;
    mag_t *sb_mag = add_mag(b0_mag, b0_len, b1_mag, b1_len, sb_len);

    len_t s0_len;
    mag_t *s0_mag = optimaly_multiply_mag(sa_mag, sa_len, sb_mag, sb_len, s0_len);

    len_t s1_len;
    mag_t *s1_mag = subtract_mag(s0_mag, s0_len, z0_mag, z0_len, s1_len);
    len_t z1_len;
    mag_t *z1_mag = subtract_mag(s1_mag, s1_len, z2_mag, z2_len, z1_len);

    free(s0_mag);
    free(s1_mag);

    // left-shift magnitude of z2
    len_t z1m_len = z1_len + half_len;
    mag_t *z1m_mag = new mag_t[z1m_len];
    for (int i = 0; i < half_len; i++) z1m_mag[i] = 0;
    memcpy(z1m_mag + half_len, z1_mag, sizeof(mag_t) * z1_len);
    z1m_len = striped_zeros_len(z1m_len, z1m_mag);

    // left-shift magnitude of z1
    len_t z2m_len = z2_len + 2*half_len;
    mag_t *z2m_mag = new mag_t[z2m_len];
    for (int i = 0; i < 2*half_len; i++) z2m_mag[i] = 0;
    memcpy(z2m_mag + 2*half_len, z2_mag, sizeof(mag_t) * z2_len);
    z2m_len = striped_zeros_len(z2m_len, z2m_mag);

    len_t z01_len;
    mag_t *z01_mag = add_mag(z0_mag, z0_len, z1m_mag, z1m_len, z01_len);

    len_t z012_len;
    mag_t *z012_mag = add_mag(z01_mag, z01_len, z2m_mag, z2m_len, z012_len);
    free(z0_mag);
    free(z1_mag);
    free(z2_mag);
    free(z1m_mag);
    free(z2m_mag);
    free(z01_mag);
    len = z012_len;
    return z012_mag;
}

tuple<mag_t *, mag_t *> divide_knuth_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len,
                                                     len_t& q_len, len_t& r_len) {
    if (a_len < b_len) {
        q_len = 0;
        r_len = a_len;
        mag_t *q_mag = nullptr;
        mag_t *r_mag = new mag_t[a_len];
        memcpy(r_mag, a_mag, a_len * sizeof(mag_t));
        return make_tuple(q_mag, r_mag);
    }
    q_len = a_len - b_len + 1;
    r_len = b_len;
    mag_t *q_mag = new mag_t[q_len];
    mag_t *r_mag = new mag_t[r_len];
    int valid = divmnu(q_mag, r_mag, a_mag, b_mag, a_len, b_len);
    if (valid != 0) {
        throw new runtime_error("Invalid division argument");
    }
    return make_tuple(q_mag, r_mag);
}

int compare_mag(const mag_t* a_mag, len_t a_len, const mag_t* b_mag, len_t b_len) {
    if (a_len == b_len) {
        for (int i = 0; i < a_len; i++) {
            if (a_mag[i] != b_mag[i]) return ((long) a_mag[i] - (long) b_mag[i]) < 0 ? -1 : 1;
        }
        return 0;
    }
    return ((long) a_len - (long) b_len) < 0 ? -1 : 1;
}

len_t striped_zeros_len(len_t n, const mag_t* mag) {
    for (int i = n - 1; i >= 0; i--) {
        if (mag[i] != 0) {
            return i + 1;
        }
    }
    return 0;
}

/**
 * Adapted from: https://github.com/hcs0/Hackers-Delight/blob/master/divmnu64.c.
 * Special thanks to: Henry S Warren.
 *
 * q[0], r[0], u[0], and v[0] contain the LEAST significant words.
 * (The sequence is in little-endian order).
 *
 * This is a fairly precise implementation of Knuth's Algorithm D, for a
 * binary computer with base b = 2**32. The caller supplies:
 *    1. Space q for the quotient, m - n + 1 words (at least one).
 *    2. Space r for the remainder (optional), n words.
 *    3. The dividend u, m words, m >= 1.
 *    4. The divisor v, n words, n >= 2.
 *   The most significant digit of the divisor, v[n-1], must be nonzero.  The
 *   dividend u may have leading zeros; this just makes the algorithm take
 *   longer and makes the quotient contain more leading zeros.  A value of
 *   NULL may be given for the address of the remainder to signify that the
 *   caller does not want the remainder.
 *      The program does not alter the input parameters u and v.
 *      The quotient and remainder returned may have leading zeros.  The
 *   function itself returns a value of 0 for success and 1 for invalid
 *   parameters (e.g., division by 0).
 *      For now, we must have m >= n.  Knuth's Algorithm D also requires
 *   that the dividend be at least as long as the divisor.  (In his terms,
 *   m >= 0 (unstated).  Therefore m+n >= n.)
 */
int divmnu(unsigned q[], unsigned r[],
           const unsigned u[], const unsigned v[],
           int m, int n) {

    const unsigned long long b = 4294967296LL; // Number base (2**32).
    unsigned *un, *vn;                         // Normalized form of u, v.
    unsigned long long qhat;                   // Estimated quotient digit.
    unsigned long long rhat;                   // A remainder.
    unsigned long long p;                      // Product of two digits.
    long long t, k;
    int s, i, j;

    if (m < n || n <= 0 || v[n-1] == 0)
        return 1;                         // Return if invalid param.

    if (n == 1) {                        // Take care of
        k = 0;                            // the case of a
        for (j = m - 1; j >= 0; j--) {    // single-digit
            q[j] = (k*b + u[j])/v[0];      // divisor here.
            k = (k*b + u[j]) - q[j]*v[0];
        }
        if (r != NULL) r[0] = k;
        return 0;
    }

    /* Normalize by shifting v left just enough so that its high-order
    bit is on, and shift u left the same amount. We may have to append a
    high-order digit on the dividend; we do that unconditionally. */

    s = nlz(v[n-1]);             // 0 <= s <= 31.
    vn = (unsigned *)alloca(4*n);
    for (i = n - 1; i > 0; i--)
        vn[i] = (v[i] << s) | ((unsigned long long)v[i-1] >> (32-s));
    vn[0] = v[0] << s;

    un = (unsigned *)alloca(4*(m + 1));
    un[m] = (unsigned long long)u[m-1] >> (32-s);
    for (i = m - 1; i > 0; i--)
        un[i] = (u[i] << s) | ((unsigned long long)u[i-1] >> (32-s));
    un[0] = u[0] << s;

    for (j = m - n; j >= 0; j--) {       // Main loop.
        // Compute estimate qhat of q[j].
        qhat = (un[j+n]*b + un[j+n-1])/vn[n-1];
        rhat = (un[j+n]*b + un[j+n-1]) - qhat*vn[n-1];
        again:
        if (qhat >= b || qhat*vn[n-2] > b*rhat + un[j+n-2])
        { qhat = qhat - 1;
            rhat = rhat + vn[n-1];
            if (rhat < b) goto again;
        }

        // Multiply and subtract.
        k = 0;
        for (i = 0; i < n; i++) {
            p = qhat*vn[i];
            t = un[i+j] - k - (p & 0xFFFFFFFFLL);
            un[i+j] = t;
            k = (p >> 32) - (t >> 32);
        }
        t = un[j+n] - k;
        un[j+n] = t;

        q[j] = qhat;              // Store quotient digit.
        if (t < 0) {              // If we subtracted too
            q[j] = q[j] - 1;       // much, add back.
            k = 0;
            for (i = 0; i < n; i++) {
                t = (unsigned long long)un[i+j] + vn[i] + k;
                un[i+j] = t;
                k = t >> 32;
            }
            un[j+n] = un[j+n] + k;
        }
    } // End j.
    // If the caller wants the remainder, unnormalize
    // it and pass it back.
    if (r != NULL) {
        for (i = 0; i < n-1; i++)
            r[i] = (un[i] >> s) | ((unsigned long long)un[i+1] << (32-s));
        r[n-1] = un[n-1] >> s;
    }
    return 0;
}

int nlz(unsigned int x) {
    int n;

    if (x == 0) return(32);
    n = 0;
    if (x <= 0x0000FFFF) {n = n +16; x = x <<16;}
    if (x <= 0x00FFFFFF) {n = n + 8; x = x << 8;}
    if (x <= 0x0FFFFFFF) {n = n + 4; x = x << 4;}
    if (x <= 0x3FFFFFFF) {n = n + 2; x = x << 2;}
    if (x <= 0x7FFFFFFF) {n = n + 1;}
    return n;
}
