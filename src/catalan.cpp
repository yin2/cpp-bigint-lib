#include <iostream>
#include "bigint.h"

using namespace std;

BigInt combinations(int n, int k) {
    BigInt c(1);
    for (int i = n; i > k; i--) {
        BigInt bi(i);
        c = c * bi;
    }
    for (int i = 2; i <= k; i++) {
        BigInt bi(i);
        c = c / bi;
    }
    return c;
};

BigInt catalan(int n) {
    BigInt c = combinations(2 * n, n);
    BigInt nplus1 = BigInt(n + 1);
    return c / nplus1;
};

int main() {
    int n;
    cin >> n;
    BigInt c = catalan(n);
    cout << bigint_fmt::radix(10) << c << endl;
}
