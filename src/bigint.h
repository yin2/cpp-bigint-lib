#ifndef TUTORIAL_BIGINT_H
#define TUTORIAL_BIGINT_H

#include <iostream>
#include <memory>
#include <tuple>

using namespace std;

typedef signed char signum_t;
typedef unsigned int len_t;
typedef unsigned int mag_t;
typedef unsigned long longmag_t;

namespace bigint_fmt {
    struct Radix_T {
        int radix;
    };

    Radix_T radix(int radix);
    ostream &operator<<(ostream &os, Radix_T radix);
}

class BigInt {
//TODO(yin): Make private to insure immutability and civil usage.
public:
    signum_t signum;
    len_t len;
    std::shared_ptr<const mag_t> mag_ptr;

    longmag_t toLongmag(long mask);
    mag_t toIntmag(int mask);

public:
    explicit BigInt();

    BigInt(int x);
    BigInt(uint x);
    BigInt(long x);
    BigInt(ulong x);

    BigInt(signum_t signum, mag_t mag);

    BigInt(signum_t signum, len_t len, mag_t const *mag);

    BigInt(signum_t signum, len_t len, shared_ptr<const mag_t> mag_ptr);

    BigInt(const BigInt& other);

    inline const mag_t* mag() {
        this->mag_ptr.get();
    }

    pair<BigInt, BigInt> divide(BigInt b);

    char toChar();
    unsigned char toUChar();
    char toShort();
    unsigned char toUShort();
    int toInt();
    unsigned int toUInt();
    long toLong();
    unsigned long toULong();

};

ostream& operator<<(ostream &out, BigInt &n);
BigInt operator+(BigInt a, BigInt b);
BigInt operator-(BigInt a, BigInt b);
BigInt operator*(BigInt a, BigInt b);
BigInt operator/(BigInt a, BigInt b);
BigInt operator%(BigInt a, BigInt b);

#endif
