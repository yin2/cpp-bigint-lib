#include <iostream>
#include <climits>
#include "bigint.h"

using namespace std;

const int radixi = ios_base::xalloc();

namespace bigint_fmt {

    Radix_T radix(int radix) {
        return {radix};
    }

    ostream &operator<<(ostream &os, Radix_T radix) {
        os.iword(radixi) = radix.radix;
        return os;
    }
}

pair<unsigned int, unsigned long> digits_fitting_ulong_and_max_value(int radix) {
    unsigned long limit = ULONG_MAX / radix;
    unsigned long acc = 1;
    int i;
    for (i = 0; acc < limit; acc *= radix, i++);
    return make_pair(i, acc);
}

char _to_digit(int d) {
    if (d < 0) {
        throw runtime_error("Invalid digit conversion argument");
    }
    if (d <= 9) {
        return '0' + d;
    }
    if (d <= 36) {
        return 'a' - 10 + d;
    }
    throw runtime_error("Invalid digit conversion argument");
}

void _print_ulong_in_radix(ostream &out, ulong n, int radix, int min_width) {
    char buf[65], *p = buf + 63;
    int r = 0;
    do {
        r = n % radix;
        n /= radix;
        *p-- = _to_digit(r);
    } while (n > 0);
    while ((long) p - (long) buf < min_width) {
        *p-- = _to_digit(0);
    }
    buf[64] = 0;
    out << ++p;
}

void _print_bigint_in_radix(ostream &out, BigInt &n, int radix, BigInt divisor, uint digits) {
    pair<BigInt, BigInt> quotient_and_remainder = n.divide(divisor);
    BigInt remainder = quotient_and_remainder.second;
    BigInt quotient = quotient_and_remainder.first;
    if (!quotient.signum == 0) {
        _print_bigint_in_radix(out, quotient, radix, divisor, digits);
    }
    long r = remainder.toULong();
    _print_ulong_in_radix(out, r, radix, quotient.signum == 0 ? 0 : digits);
}

void print_bigint_in_radix(ostream &out, BigInt &n, int radix) {
    pair<uint, ulong> digits_and_max = digits_fitting_ulong_and_max_value(radix);
    if (n.signum < 0) {
        out << '-';
    }
    _print_bigint_in_radix(out, n, radix, digits_and_max.second, digits_and_max.first);
}

ostream &operator<<(ostream &out, BigInt &n) {
    auto radix = out.iword(radixi);
    print_bigint_in_radix(out, n, radix);
    return out;
}
