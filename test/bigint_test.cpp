#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../src/bigint.h"

using testing::ElementsAreArray;

extern int karatsuba_limit;

#define ASSERT_BIGINT_EQ(actual, expected_signum, ...) { \
    BigInt result = actual;                             \
    mag_t elems[] __VA_ARGS__;                           \
    len_t len = sizeof(elems) / sizeof(*elems);          \
    if (len == 1 && elems[0] == 0x0) len = 0;            \
    ASSERT_EQ(result.signum, expected_signum);           \
    ASSERT_EQ(result.len, len);                          \
    if (len != 0) {                                      \
        auto actual_mag = vector<mag_t>(result.mag(), result.mag() + result.len); \
        ASSERT_THAT(actual_mag, ElementsAreArray(elems));\
    } else {                                             \
        ASSERT_EQ(actual.mag(), nullptr);                  \
    }                                                    \
}

class BigIntTest : public ::testing::Test {
protected:
    // Variable naming schema:
    //  1. Start with `n` as in "number"
    //  2. Followed by value expression, having 1, 2, or more terms. Operators separate the terms:
    //    a. `a` is addition as in "add"
    //    b. `t` is substraction as in "take"
    //  3. Each term is a number expressed literally or as an exponential expression: base `e` exponent
    BigInt n1 = BigInt(1);
    BigInt n2e10 = BigInt(1024);
    BigInt n2e32t1 = BigInt(1, {0xFFFFFFFF});
    BigInt n2e32 = BigInt(1, 2, new unsigned int[2]{0x0, 0x1});
    BigInt n2e32a1 = BigInt(1, 2, new unsigned int[2]{0x1, 0x1});
    BigInt n2e63 = BigInt(1, 2, new unsigned int[2]{
            0x00000000, 0x80000000
    });
    BigInt n2e64t1 = BigInt(1, 2, new unsigned int[2]{
            0xFFFFFFFF, 0xFFFFFFFF
    });
    BigInt n2e287 = BigInt(1, 9, new unsigned int[9]{
            0x00000000, 0x00000000, 0x00000000, 0x00000000,
            0x00000000, 0x00000000, 0x00000000, 0x00000000,
            0x80000000
    });
    BigInt n2e288t1 = BigInt(1, 9, new unsigned int[9]{
            0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
            0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
            0xFFFFFFFF
    });
};

TEST_F(BigIntTest, ConstructorN1) {
    ASSERT_BIGINT_EQ(n2e10, 1, { 0x400 });
}
TEST_F(BigIntTest, ConstructorN2) {
    ASSERT_BIGINT_EQ(n2e32a1, 1, { 0x1, 0x1 });
}

TEST_F(BigIntTest, AdditionN3N3) {
    ASSERT_BIGINT_EQ((n2e32 + n2e32), 1, { 0x00000000, 0x2 });
}
TEST_F(BigIntTest, AdditionN3N4) {
    ASSERT_BIGINT_EQ((n2e32 + n2e32t1), 1, { 0xFFFFFFFF, 0x1 });
}
TEST_F(BigIntTest, AdditionN3N5) {
    ASSERT_BIGINT_EQ((n2e32 + n1), 1, { 0x1, 0x1 });
}
TEST_F(BigIntTest, AdditionN4N3) {
    ASSERT_BIGINT_EQ((n2e32t1 + n2e32), 1, { 0xFFFFFFFF, 0x1 });
}
TEST_F(BigIntTest, AdditionN4N4) {
    ASSERT_BIGINT_EQ((n2e32t1 + n2e32t1), 1, { 0xFFFFFFFE, 0x1 });
}
TEST_F(BigIntTest, AdditionN4N5) {
    ASSERT_BIGINT_EQ((n2e32t1 + n1), 1, { 0x0, 0x1 });
}
TEST_F(BigIntTest, AdditionN5N3) {
    ASSERT_BIGINT_EQ((n1 + n2e32), 1, { 0x1, 0x1 });
}
TEST_F(BigIntTest, AdditionN5N4) {
    ASSERT_BIGINT_EQ((n1 + n2e32t1), 1, { 0x0, 0x1 });
}
TEST_F(BigIntTest, AdditionN5N5) {
    ASSERT_BIGINT_EQ((n1 + n1), 1, { 0x2 });
}

TEST_F(BigIntTest, AdditionN6N6) {
    ASSERT_BIGINT_EQ((n2e63 + n2e63), 1, { 0x00000000, 0x00000000, 0x00000001 });
}
TEST_F(BigIntTest, AdditionN6N7) {
    ASSERT_BIGINT_EQ((n2e63 + n2e64t1), 1, { 0xffffffff, 0x7fffffff, 0x00000001 });
}
TEST_F(BigIntTest, AdditionN7N6) {
    ASSERT_BIGINT_EQ((n2e64t1 + n2e63), 1, { 0xffffffff, 0x7fffffff, 0x00000001 });
}
TEST_F(BigIntTest, AdditionN7N7) {
    ASSERT_BIGINT_EQ((n2e64t1 + n2e64t1), 1, { 0xfffffffe, 0xffffffff, 0x00000001 });
}

TEST_F(BigIntTest, AdditionN8N8) {
    ASSERT_BIGINT_EQ((n2e287 + n2e287), 1, {
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x1
    });
}
TEST_F(BigIntTest, AdditionN8N9) {
    ASSERT_BIGINT_EQ((n2e287 + n2e288t1), 1, {
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0x7FFFFFFF, 0x1
    });
}
TEST_F(BigIntTest, AdditionN9N8) {
    ASSERT_BIGINT_EQ((n2e288t1 + n2e287), 1, {
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0x7FFFFFFF, 0x1
    });
}
TEST_F(BigIntTest, AdditionN9N9) {
    ASSERT_BIGINT_EQ((n2e288t1 + n2e288t1), 1, {
        0xFFFFFFFe, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0x1
    });
}

TEST_F(BigIntTest, SubstractionN3N3) {
    ASSERT_BIGINT_EQ((n2e32 - n2e32), 0, { 0x0 });
}
TEST_F(BigIntTest, SubstractionN3N4) {
    ASSERT_BIGINT_EQ((n2e32 - n2e32t1), 1, { 0x1 });
}
TEST_F(BigIntTest, SubstractionN3N5) {
    ASSERT_BIGINT_EQ((n2e32 - n1), 1, { 0xFFFFFFFF });
}
TEST_F(BigIntTest, SubstractionN4N3) {
    ASSERT_BIGINT_EQ((n2e32t1 - n2e32), -1, { 0x1 });
}
TEST_F(BigIntTest, SubstractionN4N4) {
    ASSERT_BIGINT_EQ((n2e32t1 - n2e32t1), 0, { 0x0 });
}
TEST_F(BigIntTest, SubstractionN4N5) {
    ASSERT_BIGINT_EQ((n2e32t1 - n1), 1, { 0xFFFFFFFE });
}
TEST_F(BigIntTest, SubstractionN5N3) {
    ASSERT_BIGINT_EQ((n1 - n2e32), -1, { 0xFFFFFFFF });
}
TEST_F(BigIntTest, SubstractionN5N4) {
    ASSERT_BIGINT_EQ((n1 - n2e32t1), -1, { 0xFFFFFFFE });
}
TEST_F(BigIntTest, SubstractionN5N5) {
    ASSERT_BIGINT_EQ((n1 - n1), 0, { 0x0 });
}

TEST_F(BigIntTest, SubstractionN6N6) {
    ASSERT_BIGINT_EQ((n2e63 - n2e63), 0, { 0x0 });
}
TEST_F(BigIntTest, SubstractionN6N7) {
    ASSERT_BIGINT_EQ((n2e63 - n2e64t1), -1, { 0xffffffff, 0x7fffffff });
}
TEST_F(BigIntTest, SubstractionN7N6) {
    ASSERT_BIGINT_EQ((n2e64t1 - n2e63), 1, { 0xffffffff, 0x7fffffff });
}
TEST_F(BigIntTest, SubstractionN7N7) {
    ASSERT_BIGINT_EQ((n2e64t1 - n2e64t1), 0, { 0x0 });
}

TEST_F(BigIntTest, SubstractionN8N8) {
    ASSERT_BIGINT_EQ((n2e287 - n2e287), 0, { 0x0 });
}
TEST_F(BigIntTest, SubstractionN8N9) {
    ASSERT_BIGINT_EQ((n2e287 - n2e288t1), -1, {
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0x7FFFFFFF
    });
}
TEST_F(BigIntTest, SubstractionN9N8) {
    ASSERT_BIGINT_EQ((n2e288t1 - n2e287), 1, {
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0x7FFFFFFF
    });
}
TEST_F(BigIntTest, SubstractionN9N9) {
    ASSERT_BIGINT_EQ((n2e288t1 - n2e288t1), 0, { 0x0 });
}

// Test modulus for divident shorter than divisor: a % b, |a| < |b|
TEST_F(BigIntTest, DivisionN1N9) {
    ASSERT_BIGINT_EQ((n2e10 / n2e288t1), 0, { 0x0 });
}
TEST_F(BigIntTest, DivisionN3N3) {
    ASSERT_BIGINT_EQ((n2e32 / n2e32), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN3N4) {
    ASSERT_BIGINT_EQ((n2e32 / n2e32t1), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN3N5) {
    ASSERT_BIGINT_EQ((n2e32 / n1), 1, { 0x0, 0x1 });
}
TEST_F(BigIntTest, DivisionN4N4) {
    ASSERT_BIGINT_EQ((n2e32t1 / n2e32t1), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN4N5) {
    ASSERT_BIGINT_EQ((n2e32t1 / n1), 1, { 0xffffffff });
}
TEST_F(BigIntTest, DivisionN5N5) {
    ASSERT_BIGINT_EQ((n1 / n1), 1, { 0x1 });
}

TEST_F(BigIntTest, DivisionN6N6) {
    ASSERT_BIGINT_EQ((n2e63 / n2e63), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN6N7) {
    n2e63 / n2e64t1;
    ASSERT_BIGINT_EQ((n2e63 / n2e64t1), 0, { 0x0 });
}
TEST_F(BigIntTest, DivisionN7N6) {
    ASSERT_BIGINT_EQ((n2e64t1 / n2e63), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN75N6) {
    ASSERT_BIGINT_EQ(((n2e64t1 + n1) / n2e63), 1, { 0x2 });
}
TEST_F(BigIntTest, DivisionN7N7) {
    ASSERT_BIGINT_EQ((n2e64t1 / n2e64t1), 1, { 0x1 });
}

TEST_F(BigIntTest, DivisionN8N8) {
    ASSERT_BIGINT_EQ((n2e287 / n2e287), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN8N9) {
    ASSERT_BIGINT_EQ((n2e287 / n2e288t1), 0, { 0x0 });
}
TEST_F(BigIntTest, DivisionN9N8) {
    ASSERT_BIGINT_EQ((n2e288t1 / n2e287), 1, { 0x1 });
}
TEST_F(BigIntTest, DivisionN95N8) {
    ASSERT_BIGINT_EQ(((n2e288t1 + n1) / n2e287), 1, { 0x2 });
}
TEST_F(BigIntTest, DivisionN9N9) {
    ASSERT_BIGINT_EQ((n2e288t1 / n2e288t1), 1, { 0x1 });
}

// Test modulus for divident shorter than divisor: a % b, |a| < |b|
TEST_F(BigIntTest, ModulusN1N9) {
    ASSERT_BIGINT_EQ((n2e10 % n2e288t1), 1, { 0x400 });
}
TEST_F(BigIntTest, ModulusN3N3) {
    ASSERT_BIGINT_EQ((n2e32 % n2e32), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN3N4) {
    ASSERT_BIGINT_EQ((n2e32 % n2e32t1), 1, { 0x1 });
}
TEST_F(BigIntTest, ModulusN3N5) {
    ASSERT_BIGINT_EQ((n2e32 % n1), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN4N4) {
    ASSERT_BIGINT_EQ((n2e32t1 % n2e32t1), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN4N5) {
    ASSERT_BIGINT_EQ((n2e32t1 % n1), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN5N5) {
    ASSERT_BIGINT_EQ((n1 % n1), 0, { 0x0 });
}

TEST_F(BigIntTest, ModulusN6N6) {
    ASSERT_BIGINT_EQ((n2e63 % n2e63), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN6N7) {
    ASSERT_BIGINT_EQ((n2e63 % n2e64t1), 1, {
        0x00000000, 0x80000000
    });
}
TEST_F(BigIntTest, ModulusN7N7) {
    ASSERT_BIGINT_EQ((n2e64t1 % n2e64t1), 0, { 0x0 });
}

TEST_F(BigIntTest, ModulusN8N8) {
    ASSERT_BIGINT_EQ((n2e287 % n2e287), 0, { 0x0 });
}
TEST_F(BigIntTest, ModulusN8N9) {
    ASSERT_BIGINT_EQ((n2e287 % n2e288t1), 1, {
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x80000000,
    });
}
TEST_F(BigIntTest, ModulusN9N8) {
    ASSERT_BIGINT_EQ((n2e288t1 % n2e287), 1, {
        0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
        0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff,
        0x7fffffff
    });
}
TEST_F(BigIntTest, ModulusN9N9) {
    ASSERT_BIGINT_EQ((n2e288t1 % n2e288t1), 0, { 0x0 });
}
